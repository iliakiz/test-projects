package com.example.serviceb.controllers;

import com.example.serviceb.feignClient.ServiceAFeignClient;
import com.example.serviceb.models.Response;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/account")
public class AccountController {

    private final ServiceAFeignClient serviceAFeignClient;

    @Operation(summary = "получение счета водителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "412", description = "Водителя с таким номером паспорта нет!")
    })
    @PostMapping("/getAccountByPassport")
    public Response<String> getAccountByPassport(@RequestParam String passport, String type) {
        Double account = serviceAFeignClient.getDriverAccountByPassport(passport).getBody();
        switch (type) {
            case "green" : account *= 2.5;
            case "blue" : account *= 1.5;
        }
        return Response.ok(type + ":" + account);
    }

    @Operation(summary = "пополнение счета водителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "412", description = "Водителя с таким номером паспорта нет!")
    })
    @PutMapping("/addAccountByPassport")
    public Response<String> addAccountByPassport(@RequestParam String passport, String type, Double sum) {
        Double account = sum;
        switch (type) {
            case "green" : account /= 2.5;
            case "blue" : account /= 1.5;
        }
        serviceAFeignClient.addDriverAccountByPassport(passport, account);
        return Response.ok(type + ":" + account);
    }

    @Operation(summary = "снятие со счета водителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "412", description = "Водителя с таким номером паспорта нет!")
    })
    @PutMapping("/changeAccountByPassport")
    public Response<String> changeAccountByPassport(@RequestParam String passport, String type, Double sum) {
        Double account = sum;
        switch (type) {
            case "green" : account /= 2.5;
            case "blue" : account /= 1.5;
        }
        serviceAFeignClient.changeDriverAccountByPassport(passport, account);
        return Response.ok(type + ":" + account);
    }
}
