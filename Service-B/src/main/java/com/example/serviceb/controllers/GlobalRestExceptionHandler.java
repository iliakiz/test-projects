package com.example.serviceb.controllers;

import com.example.serviceb.models.Response;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalRestExceptionHandler {

    @ResponseBody
    @ExceptionHandler(ResponseStatusException.class)
    public <T> Response<String> handleConflict(ResponseStatusException ex) {
        return Response.error(ex.getReason(), ex.getStatusCode());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public <T> Response<String> onConstraintValidationException(ConstraintViolationException e) {
        String message = e.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .reduce(",\n", String::concat);
        return Response.error(message, HttpStatus.BAD_REQUEST);
    }
}
