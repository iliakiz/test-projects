package com.example.serviceb.models;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

public class Response<T> extends ResponseEntity<T> {

    public Response(T data, HttpStatusCode status) {
        super(data, status);

    }

    public static <T> Response<T> ok(T data) {
        return new Response<>(data, HttpStatus.OK);
    }

    public static <T> Response<T> error(T data, HttpStatusCode status) {
        return new Response<>(data, status);
    }
}
