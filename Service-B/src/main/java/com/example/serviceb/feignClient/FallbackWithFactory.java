package com.example.serviceb.feignClient;

import com.example.serviceb.models.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatusCode;

@Slf4j
public record FallbackWithFactory(String reason) implements ServiceAFeignClient {

    public Response<Double> getDriverAccountByPassport(String passport) {
        log.info("Failed send request, reason {}", reason);
        return Response.error(-1D, HttpStatusCode.valueOf(400));
    }

    @Override
    public Response<Double> addDriverAccountByPassport(String passport, Double sum) {
        log.info("Failed send request, reason {}", reason);
        return Response.error(-1D, HttpStatusCode.valueOf(400));
    }

    @Override
    public Response<Double> changeDriverAccountByPassport(String passport, Double sum) {
        log.info("Failed send request, reason {}", reason);
        return Response.error(-1D, HttpStatusCode.valueOf(400));
    }

}
