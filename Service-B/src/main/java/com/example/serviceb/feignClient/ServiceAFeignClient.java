package com.example.serviceb.feignClient;

import com.example.serviceb.models.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "SERVICE-A", fallbackFactory = FallBack.class)
public interface ServiceAFeignClient {

    @PostMapping("api/driver/getDriverAccountByPassport")
    Response<Double> getDriverAccountByPassport(String passport);

    @PutMapping("/addDriverAccountByPassport")
    Response<Double> addDriverAccountByPassport(String passport, Double sum);

    @PutMapping("/changeDriverAccountByPassport")
    Response<Double> changeDriverAccountByPassport(String passport, Double sum);

}

