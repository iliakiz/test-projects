package com.example.service_a.models.mapper;

import com.example.service_a.models.dto.PartDto;
import com.example.service_a.models.entity.Part;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PartMapper {


    PartDto toDto(Part part);


    Part toEntity(PartDto partDto);

    List<PartDto> toListDto(List<Part> part);
}
