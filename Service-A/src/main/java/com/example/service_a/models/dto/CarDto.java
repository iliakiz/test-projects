package com.example.service_a.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

import java.time.YearMonth;

@Builder
public record CarDto(

        @NotBlank
        String vin,
        @NotBlank
        String number,

        String vendor,

        String type,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM.yyyy")
        YearMonth manufacturer
) {
}
