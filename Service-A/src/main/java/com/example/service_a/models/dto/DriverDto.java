package com.example.service_a.models.dto;

import jakarta.validation.constraints.NotBlank;

import java.time.LocalDate;

public record DriverDto(
        @NotBlank
        String fullName,
        @NotBlank
        String passport,

        String category,

        LocalDate birthday,

        Long driveYears,

        Double account
) {
}
