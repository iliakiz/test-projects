package com.example.service_a.models.mapper;

import com.example.service_a.models.dto.DriverDto;
import com.example.service_a.models.entity.Driver;
import org.mapstruct.Mapper;

import java.time.LocalDate;
import java.util.List;

@Mapper(componentModel = "spring")
public interface DriverMapper {


    DriverDto toDto(Driver driver);


    Driver toEntity(DriverDto driverDto);

    List<DriverDto> toListDto(List<Driver> drivers);
}
