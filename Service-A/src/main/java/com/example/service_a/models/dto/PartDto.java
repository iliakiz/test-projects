package com.example.service_a.models.dto;

import jakarta.validation.constraints.NotBlank;

public record PartDto(

        @NotBlank
        String name,
        @NotBlank
        String serialNo
) {
}
