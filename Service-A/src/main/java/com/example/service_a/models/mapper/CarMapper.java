package com.example.service_a.models.mapper;

import com.example.service_a.models.dto.CarDto;
import com.example.service_a.models.entity.Car;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

@Mapper(componentModel = "spring", imports = {LocalDate.class, YearMonth.class})
public interface CarMapper {

    @Mapping(target = "manufacturer", expression = "java(YearMonth.from(car.getManufacturer()))")
    CarDto toDto(Car car);

    @Mapping(target = "manufacturer", expression = "java(carDto.manufacturer().atDay(1))")
    Car toEntity(CarDto carDto);

    List<CarDto> toListDto(List<Car> cars);
}
