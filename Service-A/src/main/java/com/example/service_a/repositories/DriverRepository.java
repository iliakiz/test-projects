package com.example.service_a.repositories;

import com.example.service_a.models.entity.Car;
import com.example.service_a.models.entity.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

    boolean existsByPassport(String passport);

    @Query("""
            SELECT count(d) = 0 FROM Driver d WHERE (d.passport = :passport AND d.fullName <> :fullName)
            """)
    boolean existsByPassportExceptCurrent(String passport, String fullName);

    void deleteByPassport(String passport);

    Driver findByPassport(String passport);

    List<Driver> findAll();

}
