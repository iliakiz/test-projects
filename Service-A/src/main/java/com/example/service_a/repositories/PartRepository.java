package com.example.service_a.repositories;

import com.example.service_a.models.entity.Driver;
import com.example.service_a.models.entity.Part;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartRepository extends JpaRepository<Part, Long> {

    boolean existsBySerialNo(String serialNo);

    void deleteBySerialNo(String serialNo);

    @Query("""
            SELECT p FROM Part p JOIN Car c ON p.car.id = c.id WHERE c.number = :number
""")
    List<Part> findPartsByCarNumber(String number);

    Part findPartBySerialNo(String serialNo);

}
