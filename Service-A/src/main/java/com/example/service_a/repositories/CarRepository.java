package com.example.service_a.repositories;

import com.example.service_a.models.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    boolean existsByNumberOrVin(String number, String vin);

    @Query("""
            SELECT count(c) = 0 FROM Car c WHERE (c.number <> :number AND c.vin = :vin)
            """)
    boolean existsByVinExceptCurrent(String number, String vin);

    boolean existsByNumber(String number);


    void deleteByNumber(String number);

    Car findByNumber(String number);

    List<Car> findAll();
}
