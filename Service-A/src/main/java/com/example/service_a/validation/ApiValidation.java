package com.example.service_a.validation;

import org.springframework.http.HttpStatusCode;
import org.springframework.web.server.ResponseStatusException;

public class ApiValidation {

    public static void expectedNotNull(Object object, int statusCode, String reason) {
        if (object == null) {
            throw new ResponseStatusException(HttpStatusCode.valueOf(statusCode), reason);
        }
    }

    public static void expectedNull(Object object, int statusCode, String reason) {
        if (object != null) {
            throw new ResponseStatusException(HttpStatusCode.valueOf(statusCode), reason);
        }
    }

    public static void expectedTrue(boolean val, int statusCode, String reason) {
        if (!val) {
            throw new ResponseStatusException(HttpStatusCode.valueOf(statusCode), reason);
        }
    }

    public static void expectedFalse(boolean val, int statusCode, String reason) {
        if (val) {
            throw new ResponseStatusException(HttpStatusCode.valueOf(statusCode), reason);
        }
    }
}
