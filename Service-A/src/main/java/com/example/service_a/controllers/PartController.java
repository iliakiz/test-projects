package com.example.service_a.controllers;

import com.example.service_a.models.dto.CarDto;
import com.example.service_a.models.dto.PartDto;
import com.example.service_a.models.entity.Car;
import com.example.service_a.models.entity.Part;
import com.example.service_a.models.mapper.CarMapper;
import com.example.service_a.models.mapper.PartMapper;
import com.example.service_a.models.response.Response;
import com.example.service_a.service.entity.CarService;
import com.example.service_a.service.entity.PartService;
import com.example.service_a.validation.ApiValidation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/part")
public class PartController {

    private final PartService partService;

    private final CarService carService;

    private final PartMapper partMapper;

    @Operation(summary = "добавление детали в БД")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Деталь добавлена в базу."),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "412", description = "Деталь с таким серийным номером уже есть")
    })
    @PostMapping("/createPart")
    public Response<PartDto> createCar(@Valid @RequestBody PartDto partDto) {
        ApiValidation
                .expectedFalse(partService.existsBySerialNo(partDto.serialNo()),
                        412,"Деталь с таким серийным номером уже есть");
        Part part = partService.persist(partMapper.toEntity(partDto));
        return Response.ok(partMapper.toDto(part));
    }

    @Operation(summary = "удаление детали из БД")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Деталь удалена из базы."),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "411", description = "Детали с таким серийным номером нет в БД.")
    })
    @DeleteMapping("/deletePartBySerialNo/{serialNo}")
    public Response<String> deletePart(@PathVariable String serialNo) {
        ApiValidation
                .expectedTrue(partService.existsBySerialNo(serialNo), 411,
                        "Детали с таким серийным номером нет в БД.");
        partService.deleteBySerialNo(serialNo);
        return Response.ok("Деталь " + serialNo + " удалена из базы.");
    }

    @Operation(summary = "получение списка деталей автомобиля по гос.номеру")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "411", description = "Автомобиля с таким гос.номером нет!")
    })
    @GetMapping("/getPartsByNumber/{number}")
    public Response<List<PartDto>> getParts(@PathVariable String number) {
        ApiValidation
                .expectedTrue(carService.existsByNumber(number), 411,
                        "Автомобиля с таким гос.номером нет в БД!");
        List<Part> parts = partService.findPartsByCarNumber(number);
        return Response.ok(partMapper.toListDto(parts));
    }

}
