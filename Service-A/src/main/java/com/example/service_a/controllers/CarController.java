package com.example.service_a.controllers;

import com.example.service_a.models.dto.CarDto;
import com.example.service_a.models.entity.Car;
import com.example.service_a.models.entity.Part;
import com.example.service_a.models.mapper.CarMapper;
import com.example.service_a.models.response.Response;
import com.example.service_a.service.entity.CarService;
import com.example.service_a.service.entity.PartService;
import com.example.service_a.validation.ApiValidation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/car")
public class CarController {

    private final CarService carService;

    private final PartService partService;
    private final CarMapper carMapper;

    @Operation(summary = "добавление нового автомобиля в БД")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Автомобиль добавлен в базу."),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "412", description = "Такой гос.номер/vin уже используется!")
    })
    @PostMapping("/createCar")
    public Response<String> createCar(@Valid @RequestBody CarDto carDto) {
        ApiValidation
                .expectedFalse(carService.existsByNumberOrVin(carDto.number(), carDto.vin()),
                        412,"Такой гос.номер/vin уже используется!");
        carService.persist(carMapper.toEntity(carDto));
        return Response.ok("Автомобиль " + carDto.number() + "добавлен в БД!");
    }

    @Operation(summary = "удаление автомобиля из БД")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Автомобиль удален из базы."),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "411", description = "Автомобиля с таким гос.номером нет!")
    })
    @DeleteMapping("/deleteCarByNumber/{number}")
    public Response<String> deleteCar(@PathVariable String number) {
        ApiValidation
                .expectedTrue(carService.existsByNumber(number), 411,
                        "Автомобиля с таким гос.номером нет в БД!");
        carService.deleteByNumber(number);
        return Response.ok("Автомобиль " + number + " удален из базы.");
    }

    @Operation(summary = "обновление данных автомобиля")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Данные автомобиля обновлены"),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "412", description = "Такой гос.номер/vin уже используется!")
    })
    @PutMapping("/updateCar")
    public Response<String> updateCar(@Valid @RequestBody CarDto carDto) {
        ApiValidation
                .expectedTrue(carService.existsByNumber(carDto.number()), 411,
                        "Автомобиля с таким гос.номером нет в БД!");
        ApiValidation
                .expectedTrue(carService.existsByVinExceptCurrent(carDto.number(), carDto.vin()),
                        412,"Такой vin уже используется!");
        carService.merge(carMapper.toEntity(carDto));
        return Response.ok("Данные автомобиля обновлены!");
    }

    @Operation(summary = "получение данных автомобиля по гос.номеру")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "411", description = "Автомобиля с таким гос.номером нет!")
    })
    @GetMapping("/getCarByNumber/{number}")
    public Response<CarDto> getCar(@PathVariable String number) {
        ApiValidation
                .expectedTrue(carService.existsByNumber(number), 411,
                        "Автомобиля с таким гос.номером нет в БД!");
        return Response.ok(carMapper.toDto(carService.findByNumber(number)));
    }

    @Operation(summary = "получение данных всех автомобилей из БД")
    @GetMapping("/getAllCars")
    public Response<List<CarDto>> getAllCars() {
        return Response.ok(carMapper.toListDto(carService.findAll()));
    }

    @Operation(summary = "добавление детали к автомобилю")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "411", description = "Автомобиля с таким гос.номером нет!")
    })
    @PutMapping("/addPartToCar/")
    public Response<String> addPartToCar(@RequestParam String number, String serialNo) {
        ApiValidation
                .expectedTrue(carService.existsByNumber(number), 411,
                        "Автомобиля с таким гос.номером нет в БД!");
        ApiValidation
                .expectedTrue(partService.existsBySerialNo(serialNo), 411,
                        "Детали с таким серийным номером нет в БД.");
        Part part = partService.findPartBySerialNo(serialNo);
        part.setCar(carService.findByNumber(number));
        partService.persist(part);
        return Response.ok("Деталь " + serialNo + " добавлена к автомобилю " + number);
    }


}
