package com.example.service_a.controllers;

import com.example.service_a.models.dto.CarDto;
import com.example.service_a.models.dto.DriverDto;
import com.example.service_a.models.entity.Car;
import com.example.service_a.models.entity.Driver;
import com.example.service_a.models.entity.Part;
import com.example.service_a.models.mapper.CarMapper;
import com.example.service_a.models.mapper.DriverMapper;
import com.example.service_a.models.response.Response;
import com.example.service_a.service.entity.CarService;
import com.example.service_a.service.entity.DriverService;
import com.example.service_a.validation.ApiValidation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/driver")
public class DriverController {

    private final DriverService driverService;

    private final CarService carService;

    private final DriverMapper driverMapper;

    @Operation(summary = "добавление нового водителя в БД")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Водитель добавлен в базу."),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "412", description = "Водитель с такими данными паспорта уже есть!")
    })
    @PostMapping("/createDriver")
    public Response<DriverDto> createCar(@Valid @RequestBody DriverDto driverDto) {
        ApiValidation
                .expectedFalse(driverService.existsByPassport(driverDto.passport()),
                        412,"Водитель с такими данными паспорта уже есть!");
        Driver driver = driverService.persist(driverMapper.toEntity(driverDto));
        return Response.ok(driverMapper.toDto(driver));
    }

    @Operation(summary = "удаление водителя из БД")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Водитель удален из базы."),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "411", description = "Водителя с таким номером паспорта нет")
    })
    @DeleteMapping("/deleteDriverByPassport/{passport}")
    public Response<String> deleteDriver(@PathVariable String passport) {
        ApiValidation
                .expectedTrue(driverService.existsByPassport(passport), 411,
                        "Водителя с таким номером паспорта нет!");
        driverService.deleteByPassport(passport);
        return Response.ok("Водитель с " + passport + " удален из базы.");
    }

    @Operation(summary = "обновление данных водителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Данные водителя обновлены"),
            @ApiResponse(responseCode = "400", description = "Некорректные данные переданы в ДТО."),
            @ApiResponse(responseCode = "411", description = "Водитель с такими данными паспорта уже есть!")
    })
    @PutMapping("/updateDriver")
    public Response<String> updateCar(@Valid @RequestBody DriverDto driverDto) {
        ApiValidation
                .expectedTrue(driverService.existsByPassport(driverDto.passport()), 411,
                        "Водителя с таким номером паспорта нет!");
        driverService.merge(driverMapper.toEntity(driverDto));
        return Response.ok("Данные водителя обновлены!");
    }

    @Operation(summary = "получения данных водителя по номеру паспорта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "411", description = "Водителя с таким номером паспорта нет!")
    })
    @GetMapping("/getDriverByPassport/{passport}")
    public Response<DriverDto> getDriver(@PathVariable String passport) {
        ApiValidation
                .expectedTrue(driverService.existsByPassport(passport), 411,
                        "Водителя с таким номером паспорта нет!");
        return Response.ok(driverMapper.toDto(driverService.findByPassport(passport)));
    }

    @Operation(summary = "получения данных всех водителей")
    @GetMapping("/getAllDrivers")
    public Response<List<DriverDto>> getAllDrivers() {
        return Response.ok(driverMapper.toListDto(driverService.findAll()));
    }

    @Operation(summary = "добавление автомобиля водителю")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "411", description = "Автомобиля с таким гос.номером нет!"),
            @ApiResponse(responseCode = "412", description = "Водителя с таким номером паспорта нет!")
    })
    @PutMapping("/addCarToDriver/")
    public Response<String> addPartToCar(@RequestParam String number, String passport) {
        ApiValidation
                .expectedTrue(carService.existsByNumber(number), 411,
                        "Автомобиля с таким гос.номером нет в БД!");
        ApiValidation
                .expectedTrue(driverService.existsByPassport(passport), 412,
                        "Водителя с таким номером паспорта нет!");
        Car car = carService.findByNumber(number);
        car.setDriver(driverService.findByPassport(passport));
        carService.persist(car);
        return Response.ok("Автомобиль " + number + " добавлена к водителю " + passport);
    }

    @Operation(summary = "получение суммы счета водителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "412", description = "Водителя с таким номером паспорта нет!")
    })
    @PostMapping("/getDriverAccountByPassport")
    public Response<Double> getDriverAccount(@RequestParam String passport) {
        ApiValidation
                .expectedTrue(driverService.existsByPassport(passport), 412,
                        "Водителя с таким номером паспорта нет!");
        return Response.ok(driverService.findByPassport(passport).getAccount());
    }

    @Operation(summary = "пополнение счета водителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "412", description = "Водителя с таким номером паспорта нет!")
    })
    @PutMapping("/addDriverAccountByPassport")
    public Response<Double> addDriverAccount(@RequestParam String passport, Double sum) {
        ApiValidation
                .expectedTrue(driverService.existsByPassport(passport), 412,
                        "Водителя с таким номером паспорта нет!");
        Driver driver = driverService.findByPassport(passport);
        sum += driver.getAccount();
        driver.setAccount(sum);
        driverService.persist(driver);
        return Response.ok(sum);
    }

    @Operation(summary = "снятие со счета водителя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "412", description = "Водителя с таким номером паспорта нет!")
    })
    @PutMapping("/changeDriverAccountByPassport")
    public Response<Double> changeDriverAccount(@RequestParam String passport, Double sum) {
        ApiValidation
                .expectedTrue(driverService.existsByPassport(passport), 412,
                        "Водителя с таким номером паспорта нет!");
        Driver driver = driverService.findByPassport(passport);
        sum = driver.getAccount() - sum;
        ApiValidation
                .expectedFalse(sum < 0, 413,"На счету недостаточно средств");
        driver.setAccount(sum);
        driverService.persist(driver);
        return Response.ok(sum);
    }
}
