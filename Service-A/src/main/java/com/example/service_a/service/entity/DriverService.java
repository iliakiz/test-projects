package com.example.service_a.service.entity;

import com.example.service_a.models.entity.Car;
import com.example.service_a.models.entity.Driver;

import java.util.List;

public interface DriverService {

    boolean existsByPassport(String passport);

    boolean existsByPassportExceptCurrent(String passport, String fullName);

    Driver persist(Driver driver);

    Driver merge(Driver driver);

    void deleteByPassport(String passport);

    Driver findByPassport(String passport);

    List<Driver> findAll();
}
