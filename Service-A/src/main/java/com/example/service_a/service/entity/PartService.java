package com.example.service_a.service.entity;

import com.example.service_a.models.entity.Part;

import java.util.List;

public interface PartService {

    boolean existsBySerialNo(String serialNo);

    void deleteBySerialNo(String serialNo);

    List<Part> findPartsByCarNumber(String number);

    Part persist(Part part);

    Part findPartBySerialNo(String serialNo);
}
