package com.example.service_a.service.entity.impl;

import com.example.service_a.models.entity.Car;
import com.example.service_a.models.entity.Driver;
import com.example.service_a.repositories.DriverRepository;
import com.example.service_a.service.entity.DriverService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DriverServiceImpl implements DriverService {

private final DriverRepository driverRepository;

    @Override
    public boolean existsByPassport(String passport) {
        return driverRepository.existsByPassport(passport);
    }

    @Override
    public boolean existsByPassportExceptCurrent(String passport, String fullName) {
        return driverRepository.existsByPassportExceptCurrent(passport, fullName);
    }

    @Override
    public Driver persist(Driver driver) {
        return driverRepository.save(driver);
    }

    @Override
    public Driver merge(Driver driver) {
        driver.setId(driverRepository.findByPassport(driver.getPassport()).getId());
        return driverRepository.save(driver);
    }

    @Transactional
    @Override
    public void deleteByPassport(String passport) {
        driverRepository.deleteByPassport(passport);
    }

    @Override
    public Driver findByPassport(String passport) {
        return driverRepository.findByPassport(passport);
    }

    @Override
    public List<Driver> findAll() {
        return driverRepository.findAll();
    }
}
