package com.example.service_a.service.entity.impl;

import com.example.service_a.models.entity.Car;
import com.example.service_a.models.entity.Part;
import com.example.service_a.repositories.PartRepository;
import com.example.service_a.service.entity.PartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PartServiceImpl implements PartService {

    private final PartRepository partRepository;

    @Override
    public boolean existsBySerialNo(String serialNo) {
        return partRepository.existsBySerialNo(serialNo);
    }
    @Transactional
    @Override
    public void deleteBySerialNo(String serialNo) {
        partRepository.deleteBySerialNo(serialNo);
    }

    @Override
    public List<Part> findPartsByCarNumber(String number) {
        return partRepository.findPartsByCarNumber(number);
    }

    @Override
    public Part persist(Part part) {
        return partRepository.save(part);
    }

    @Override
    public Part findPartBySerialNo(String serialNo) {
        return partRepository.findPartBySerialNo(serialNo);
    }
}
