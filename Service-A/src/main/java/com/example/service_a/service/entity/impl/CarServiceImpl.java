package com.example.service_a.service.entity.impl;

import com.example.service_a.models.entity.Car;
import com.example.service_a.repositories.CarRepository;
import com.example.service_a.service.entity.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    @Override
    public boolean existsByNumberOrVin(String number, String vin) {
        return carRepository.existsByNumberOrVin(number, vin);
    }

    @Override
    public boolean existsByVinExceptCurrent(String number, String vin) {
        return carRepository.existsByVinExceptCurrent(number, vin);
    }

    @Override
    public boolean existsByNumber(String number) {
        return carRepository.existsByNumber(number);
    }

    @Override
    public Car persist(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Car merge(Car car) {
        car.setId(carRepository.findByNumber(car.getNumber()).getId());
        return carRepository.save(car);
    }

    @Transactional
    @Override
    public void deleteByNumber(String number) {
        carRepository.deleteByNumber(number);
    }

    @Override
    public Car findByNumber(String number) {
        return carRepository.findByNumber(number);
    }

    @Override
    public List<Car> findAll() {
        return carRepository.findAll();
    }


}
