package com.example.service_a.service.entity;

import com.example.service_a.models.entity.Car;

import java.util.List;

public interface CarService {

    boolean existsByNumberOrVin(String number, String vin);


    boolean existsByVinExceptCurrent(String number, String vin);

    boolean existsByNumber(String number);

    Car persist(Car car);

    Car merge(Car car);

    void deleteByNumber(String number);

    Car findByNumber(String number);

    List<Car> findAll();
}
